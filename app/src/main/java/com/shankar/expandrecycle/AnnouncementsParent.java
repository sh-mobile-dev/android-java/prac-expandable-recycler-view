package com.shankar.expandrecycle;

import android.widget.TextView;


import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

/**
 * Created by Shankar on 04-08-2017.
 */

public class AnnouncementsParent implements ParentListItem{

    private String title;
    private List<Descript> descripts;
    public  AnnouncementsParent(){
        title = "ParentListItem";
    }

    @Override
    public List<?> getChildItemList() {
        return descripts;
    }
    public void setChildItemList(List<Descript> descripts) {
        this.descripts= descripts;
    }

    public String getTitle() {
        return title;
    }

    public List<Descript> getDescripts() {
        return descripts;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
